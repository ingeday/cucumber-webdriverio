//Complete siguiendo las instrucciones del taller

var {Given, When, Then } = require('cucumber');
var {expect} = require('chai');

Given('I go to losestudiantes home screen', ()=>{
    browser.url('/');
    if($('button=Cerrar').isDisplayed()) {
        $('button=Cerrar').click();
    }
});

When('I open the register screen', ()=>{
    $('button=Ingresar').waitForExist(5000);
    $('button=Ingresar').waitForDisplayed(5000);
    $('button=Ingresar').click();
})

When(/^I fill register with (.*) and (.*) and (.*) and (.*)$/, (name,lastname,email,password)=>{
    var cajaSignUp=$('.cajaSignUp');

    var nameInput = cajaSignUp.$('input[name="nombre"]');
    nameInput.click();
    nameInput.setValue(name);

    var lastNameInput = cajaSignUp.$('input[name="apellido"]');
    lastNameInput.click();
    lastNameInput.setValue(lastname);

    var emailInput = cajaSignUp.$('input[name="correo"]');
    emailInput.click();
    emailInput.keys(email);

    var passwordInput = cajaSignUp.$('input[name="password"]');
    passwordInput.click();
    passwordInput.keys(password)
});


When('I try to register', ()=>{
    var cajaLogIn = $('.cajaSignUp');
    cajaLogIn.$('button=Registrarse').click();
});

Then('I expect to see this {string}',(error)=>{
    $('.aviso.alert.alert-danger').waitForDisplayed(5000);
    var alertText = browser.$('.aviso.alert.alert-danger').getText();
    expect(alertText).to.include(error);
});

When('I open the login screen', ()=>{
    $('button=Ingresar').waitForExist(5000);
    $('button=Ingresar').waitForDisplayed(5000);
    $('button=Ingresar').click();
})

When(/^I fill login with (.*) and (.*)$/, (email,password)=>{
    var cajaLogIn=$('.cajaLogIn');

    var mailInput = cajaLogIn.$('input[name="correo"]');
    mailInput.click();
    mailInput.setValue(email);

    var passwordInput = cajaLogIn.$('input[name="password"]');
    passwordInput.click();
    passwordInput.keys(password)
});

When('I try to login', () => {
    var cajaLogIn = $('.cajaLogIn');
    cajaLogIn.$('button=Ingresar').click();
});

Then('I expect to see {string}', (error) => {
    $('.aviso.alert.alert-danger').waitForDisplayed(5000);
    var alertText = browser.$('.aviso.alert.alert-danger').getText();
    expect(alertText).to.include(error);
});

When(/^I made a login succesfull with (.*) and (.*)$/, (email,password) =>{
    var cajaLogIn=$('.cajaLogIn');

    var mailInput = cajaLogIn.$('input[name="correo"]');
    mailInput.click();
    mailInput.setValue(email);

    var passwordInput = cajaLogIn.$('input[name="password"]');
    passwordInput.click();
    passwordInput.keys(password);
})

Then('I expect to see a link with text Salir',()=>{
    $("a[role='menuitem']").waitForExist(5000);
    
})