Feature: Login into losestudiantes
    As an unser I want to authenticate myself within losestudiantes website in order to rate teachers

    Scenario Outline: Register Failed
        Given I go to losestudiantes home screen
        When I open the register screen
        And I fill register with <name> and <lastname> and <email> and <password>
        And I try to register
        Then I expect to see this <error>

        Examples:
            | name | lastname | email | password | error |
            | pepito  | perez  | pp@localhost.com  | 123456789 | "Debes aceptar los términos" |
            | pepito  | perez  | pp@localhost.com  |  | "Ingresa una contraseña" |
            | pepito  | perez  |   | 123456789 | "Ingresa tu correo" |