Feature: Login into losestudiantes
    As an unser I want to authenticate myself within losestudiantes website in order to rate teachers

    Background: Background name
        Given I go to losestudiantes home screen
    Scenario Outline: Login failed
        When I open the login screen
        And I fill login with <email> and <password>
        And I try to login
        Then I expect to see <error>

        Examples:
            | email | password | error |
            |  |  | "Ingresa una contraseña" |
            | miso@gmail.com | 1234 | "Upss! El correo y" |


    Scenario Outline: Login Successful
        When I open the login screen
        And I made a login succesfull with <email> and <password>
        And I try to login
        Then I expect to see a link with text Salir

        Examples:
            | email | password |
            | ea.gonzalezm@uniandes.edu.co | 1234567890 |